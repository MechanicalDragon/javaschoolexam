package com.tsystems.javaschool.tasks.subsequence;

import java.util.List;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {
        // TODO: Implement the logic here

        if (x == null || y == null){
            throw new IllegalArgumentException();
        }

//        Maybe it was meant for generics? They were here, but this way is more simple.
        int lastElement = 0;
        int matchingPairs = 0;
        for (Object t : x){
            for (int i = lastElement; i < y.size(); i++) {
                Object tt = y.get(i);
                if (t.equals(tt)){
                    lastElement = y.indexOf(tt)+1;
                    matchingPairs++;
                    break;
                }
            }
        }

        return matchingPairs == x.size();

    }

}
