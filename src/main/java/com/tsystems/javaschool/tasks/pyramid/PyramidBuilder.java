package com.tsystems.javaschool.tasks.pyramid;

import java.util.Collections;
import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        // TODO : Implement your solution here

        double rowsNumber = (-1 + Math.sqrt(1 + 8 * inputNumbers.size())) / 2;
        if (rowsNumber - (int) rowsNumber != 0) {
            throw new CannotBuildPyramidException();
        }

        int foundationLength = (int) rowsNumber * 2 - 1;
        int[][] pyramid = new int[(int) rowsNumber][foundationLength];

        try {
            Collections.sort(inputNumbers);
        } catch (NullPointerException e) {
            throw new CannotBuildPyramidException();
        }

        int axle = foundationLength / 2;
        for (int i = 0, numsInARow = 1; i < inputNumbers.size(); numsInARow++) {
            for (int numsAdded = 0; numsAdded < numsInARow; numsAdded++, i++) {
//                numsInARow equals to a number of rows
                pyramid[numsInARow - 1][axle - numsInARow + numsAdded * 2 + 1] = inputNumbers.get(i);
            }
        }

        return pyramid;
    }
}

