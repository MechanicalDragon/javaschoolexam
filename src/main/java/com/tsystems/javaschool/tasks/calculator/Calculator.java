package com.tsystems.javaschool.tasks.calculator;

import java.text.DecimalFormat;
import java.util.EmptyStackException;
import java.util.Locale;
import java.util.Stack;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing doAAction of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        // TODO: Implement the logic here

        class Operator{

            private final int priority;
            private final char arithmeticAction;

            private Operator(char c) {
                arithmeticAction = c;
                priority = ( c == '+' || c == '-') ? 1 : 2;
            }

            private double firstOperand;
            private double secondOperand;

            private double doAAction(){
                if ( arithmeticAction == '+') {
                    return firstOperand + secondOperand;
                } else {
                    if ( arithmeticAction == '-') {
                        return firstOperand - secondOperand;
                    } else {
                        if ( arithmeticAction == '*') {
                            return firstOperand * secondOperand;
                        } else {
                            if ( secondOperand == 0){
                                throw new ArithmeticException();
                            } else {
                                return firstOperand / secondOperand;
                            }
                        }
                    }
                }
            }
        }

        class CodeOptimizer {

            private String statement;

            private Stack<Double> numbers = new Stack<>();
            private Stack<Character> operators = new Stack<>();
            private String allowedSymbols = "+-*/().";
            private StringBuilder currentNumber = new StringBuilder();

            private CodeOptimizer(String statement){
                this.statement = statement;
            }

            private String evaluate() {

                for (int i = 0; i < statement.length(); i++){
                    char c = statement.charAt(i);

                    if (Character.isDigit(c)) {
                        currentNumber.append(c);

                    } else{
                        if (allowedSymbols.indexOf(c) < 0 ||
                                (i == 0 &&
                                (c != '-' && c != '(')
                                )) {
                            return null;
                        } else {
                            if (c == '.' ||
                                    (c == '-' &&
                                    (i == 0 || statement.charAt(i - 1) == '(')
                                    )) {
                                currentNumber.append(c);
                                continue;
                            }
                        }

                        if (!currentNumber.toString().equals("-")) {
                            if (pushOperand()) return null;
                        }

                        if (c == ')') {
                            if (allowedSymbols.indexOf(statement.charAt(i - 1)) < 5 &&
                                    allowedSymbols.indexOf(statement.charAt(i - 1)) > 0){
                                return null;
                            }
                            Character oper = operators.pop();
                            while (oper != '(') {
                                if (operate(new Operator(oper))) return null;
                                try {
                                    oper = operators.pop();
                                } catch (EmptyStackException e) {
                                    return null;
                                }
                            }
                        } else {
                            if (c == '(') {
                                if (i != 0 && (allowedSymbols.indexOf(statement.charAt(i - 1)) > 4 ||
                                        allowedSymbols.indexOf(statement.charAt(i - 1)) < 0)) {
                                    return null;
                                }
                                operators.push(c);
                            } else {
                                if (operators.size() > 0 && operators.peek() != '(') {
                                    Operator o1 = new Operator(c);
                                    Operator operator = new Operator(operators.peek());
                                    while (o1.priority <= operator.priority) {
                                        if (operate(operator)) return null;
                                        operators.pop();
                                        if (operators.size() == 0 || operators.peek() == '(') {
                                            break;
                                        }
                                        operator = new Operator(operators.peek());
                                    }
                                }
                                operators.push(c);
                            }
                        }
                    }
                }

                if (pushOperand()) return null;
                while (!operators.empty()){
                    if (operate(new Operator(operators.pop()))) return null;
                }

                double result = numbers.pop();
                Locale.setDefault(new Locale("en"));
                DecimalFormat decimalFormat = new DecimalFormat("#.####");
                return decimalFormat.format(result);
            }

            private boolean pushOperand() {
                if (!currentNumber.toString().isEmpty()) {
                    try {
                        numbers.add(Double.parseDouble(String.valueOf(currentNumber)));
                    } catch (NumberFormatException e) {
                        return true;
                    }
                    currentNumber.delete(0, currentNumber.length());
                    return false;
                }
                return false;
            }

            private boolean operate(Operator operator) {
                try {
                    operator.secondOperand = numbers.pop();
                    operator.firstOperand = numbers.pop();
                } catch (EmptyStackException e) {
                    return true;
                }
                try {
                    numbers.add(operator.doAAction());
                } catch (ArithmeticException e) {
                    return true;
                }
                return false;
            }
        }

        if (statement == null || statement.length() == 0){
            return null;
        } else {
            return new CodeOptimizer(statement).evaluate();
        }
    }
}
